# MobAd SDK - 2.0.0
- [Getting Started](#getting-started)
  - [Prerequisites:](#prerequisites)
  - [Installation](#installation)
    - [Swift Package Manager](#swift-package-manager)
- [Usage](#usage)
  - [MobAd Configurations](#mobad-configurations)
    - [Request Permission for ATTracking](#request-permission-for-attracking)
    - [Create a new Property List file:](#create-a-new-property-list-file)
    - [Import the module to your project:](#import-the-module-to-your-project)
    - [AppDelegate Configuration](#appdelegate-configuration)
  - [Fetch full screen ad](#fetch-full-screen-ad)
    - [SwiftUI](#swiftui)
    - [UIKit](#uikit)
  - [Get notification ad](#get-notification-ad)
  - [Update User Profile](#update-user-profile)
    - [Fetch Languages](#fetch-languages)
  - [Add/Remove interests](#addremove-interests)
    - [Add Interest](#add-interest)
    - [Remove Interest](#remove-interest)
  - [Fetch category](#fetch-category)
  - [Fetch User Profile](#fetch-user-profile)
- [MobAd.plist](#mobadplist)

# Getting Started
The following instructions will allow you to integrate MobAd SDK to your iOS application using XCode.

## Prerequisites:
- Install the following:
   - Xcode 14.1 or later
- Make sure that your project meets these requirements:
  - Your project must target these platform versions or later:
    - iOS 15.0 or later
    - iPadOS 15.0 or later

## Installation
### Swift Package Manager

Use [Swift Package Manager](https://developer.apple.com/documentation/xcode/adding-package-dependencies-to-your-app) to install and manage MobAd.


In Xcode, with your app project open, navigate to File > Add Packages.
When prompted, add *BOTH* MobAd Apple platforms SDK repositories:
```
https://gitlab.com/imw-developers/MobAdSDK-iOS.git
```
and:
```
https://gitlab.com/imw-developers/mobadsdk-ui-ios.git
```
> We recommend using the default (latest) SDK version.
# Usage

## MobAd Configurations
### Request Permission for ATTracking

In the `Info.plist` file, add following property:

```xml
<key>NSUserTrackingUsageDescription</key>
<string>This app uses your data to provide personalized advertisements.</string>
```
>  **Warning:** If the property is not added, the app will crash.
>

In case you are using `SwiftUI`, to access the `Info.plist` file, go to Targets > Info, then add the required field: `Privacy - Tracking Usage Description`.

### Create a new Property List file:

Name your file `MobAd.plist` and paste in the code (code will be provided, but here is a [template](#mobadplist)).

> **Warning:** do not track `MobAd.plist` with git.
>
> Make sure to add `MobAd.plist` to `.gitignore` file.


### Import the module to your project:

```swift
import MobAdSDK
```
> always import the module when you need to use MobAd
>
> After importing, if the module is not found, try building the project or clean build. In case of failure to get rid of the error message, go to `Target` > `Build Phases` and check if MobAdSDK is in `Embed Frameworks` or `Link Binary With Libraries`.

### AppDelegate Configuration
In your AppDelegate class, inside the `didFinishLaunchingWithOptions` function, call the `configure(for:)` function like so:

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
  
  MobAd.shared.configure(for: <#Environment#>)
  return true
}
```

And in case you are using `SwiftUI` it is the same process:
```swift
import SwiftUI
import MobAdSDK

class AppDelegate: NSObject, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        MobAd.shared.configure(for: <#Environment#>)
        return true
    }
}

@main
struct MyApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

```

In case you want to receive a response, or handle a callback:

```swift
MobAd.shared.configure(for: <#Environment#>) { responseMessage in
    //handle completion
}
```

> For `Environment`, Use `.developement` for development and testing, and `.production` for releases and updates.

And now, you can create a shared instance of MobAd with:

```swift
let mobad = MobAd.shared
```

> Please note that in order to fetch an ad or call any other function, `configure(for:)` must be called.
> 
> In case of network failure or any issue that may prevent the success of the `configure(for:)` function, all of MobAd's function will fail. But `configure(for:)` can be run again once th user re-opens the app. Just call it again whenever you need it.

## Fetch full screen ad

### SwiftUI
To fetch and advertisement, first you need to create a boolean:
```swift
@State private var adReceived = false
```
Next up, fetch the ad with:

```swift
// success : Boolean
// error: Error
 mobad.getAd { success, error in

    if let error = error {
        // handle error
    } else {

        adReceived = success
    }
}
```

And finally, display the Ad:
```swift
.fullScreenCover(isPresented: $adReceived) {
        AnyView(mobad.displayView())
}
```

In case you would like to handle some actions after the user dismisses the ad, the `onDisappear` modifier can be used and call from it your desired actions:

```swift
.fullScreenCover(isPresented: $adReceived) {
    AnyView(mobad.displayView())
        .onDisappear{
            // add functionnalities
        }
}
```
> **Warning**: Always call `getAd()` before displaying the Ad.

### UIKit
For UIKit, you can fetch an ad with the following: 

```swift
mobad.getAd { _, error in
  if let error = error {
      // handle error
  } else {
     
      DispatchQueue.main.async {[self] in
          
          let adView = self.mobad.adView
          
          present(adView, animated: true)
      }
  }
}
```

## Get notification ad 
In the Notification Delegate Class, add the following code to the `didReceive:` userNotification function:
```swift
mobad.handleMobAdNotifications(of: response)
```

The function should look like this:

```swift
func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    mobad.handleMobAdNotifications(of: response)
    completionHandler()
}
```

To schedule the notification, you can use the following lines of code:

```swift
mobad.getAd(delayInMinutes: <#Double#>) { error in
    if let error = error {
        // handle errors
    }
}
```
> Delay time should be specified in minutes
> Please note that the maximum delay is 60 minutes

In case you do not have a notification delegate class, please do the following:
- Create a new file `NotificationHandlerDelegate`.
- Paste this code in it: 
```swift
import Foundation
import UserNotifications
import MobAdSDK

class NotificationHandlerDelegate: NSObject, UNUserNotificationCenterDelegate{
    
    private let mobad = MobAd.shared
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        mobad.handleMobAdNotifications(of: response)
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.banner, .list, .sound])
    }
}
```
- And finally, in the AppDelegate:
```swift
 let notificationHandlerDelegate = NotificationHandlerDelegate()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        ...
        
        UNUserNotificationCenter.current().delegate = notificationHandlerDelegate
        return true
    }
```

## Update User Profile
To update the user's profile, you can simply use the following code: 

```swift
mobad.updateProfile(adCap: <#Int#>, isOptedOut: <#Bool#>, languageIdList: <#[String]#>)
```

Or if you want to use a completion handler :
```swift
mobad.updateProfile(adCap: <#Int#>, isOptedOut: <#Bool#>, languageIdList: <#[String]#>) { responseMessage in
    // handle completion
}
```

In `updateProfile()`, you can set:
- `adCap`: which is the maximum numbers of ad the user can receive per day (50 max).
- `isOptedOut`: if true, the user will not receive ads when `getAd` is called.
- `languageIdList`: is an array of strings that accepts Language IDs (i.e: `en` for English, `fr` for French...).

### Fetch Languages

You can let the user select their languages with the following function:

```swift
mobad.fetchLanguages { languages, error in
    if let error = error {
        // handle error
    } else {
        guard let languages = languages else {
            return
        }
        // do something
    }
}
```
> You can use a TableView or a List to list these languages for your users and then `updateProfile()` once the lanagues are selected. Or you can by default set it to a default array of languages.


## Add/Remove interests
For a personalized experience for your users, you can let the user receive an ad that may interest them by adding or removing topics, check out the interests [here](#fetch-category).

### Add Interest
```swift
mobad.addInterest(interest: <#[String]#>)
```

### Remove Interest
```swift
mobad.removeInterest(interest: <#[String]#>)
```

Both of them have completion handlers too:

```swift
mobad.addInterest(interest: <#[String]#>){ responseMessage in
    // handle completion
}


mobad.removeInterest(interest: <#[String]#>){ responseMessage in
    // handle completion
}
```

## Fetch category
You can get all the available categories by running the following:
```swift
mobad.fetchCategories { categories, error in
    if let error = error {
        // handle error
    } else {
        // handle the response.
    }
}
```
> you can create a List or a TableView of categories, and let the users select their interests.
>
> This can be useful if you want to create a form for your users and let them customize their own ad experience.

## Fetch User Profile
You can fetch the user's profile with the following:
```swift
mobad.fetchProfile { user, error in
    if let error = error {
        // handle error
    } else {
        // handle user's data.
    }
}
```

# MobAd.plist
```plist
<key>appId</key>
<string>YOUR_APP_ID</string>

<key>machineSecret</key>
<string>YOUR_MACHINE_SECRET</string>
```
> Do not forget to input the application's ID and the machine secret.


In case of any inconvenience, you're welcome to contact our support team by emailing [support@i-magineworks.com](mailto:support@i-magineworks.com)
